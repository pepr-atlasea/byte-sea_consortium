# BYTE-SEA_consortium



## Description

Members of the various teams participating in the BYTE-SEA targeted project within the ATLASEA programme

## Project coordination

Coordinating establishment : CNRS DR17 
Project supervisor : Erwan CORRE (IFB)
Project agreement no: 22-EXAT-0004

## Project Description

The ATLASea programme (PEPR Exploratoire 2023-2030, led by CNRS and CEA) proposes to decipher and exploit the colossal wealth of information from marine biodiversity along the French coast. 4500 marine species will be sampled by the DIVE-Sea Targeted Project lead by MNHN, with a high coverage of species from mainland France, but also including species from overseas territories. The samples will be entrusted to the teams of the SEQ-Sea Targeted Project  lead by CEA to produce genome assemblies associated with high-quality structural annotations. 

The BYTE-Sea Targeted Project will centralise all the genomic data produced by the ATLASea project into a single portal3 and integrate the genomes of related organisms sequenced by other consortia. It will guarantee the interoperability and security of the data, and facilitate its dissemination and use in accordance with the FAIR and Open Science principles. The environment developed will provide access to sequencing data on the genomes of marine organisms, including information on the genomic sequence, genes and coding and non-coding regions, and will offer tools for sequence similarity searching, genome analysis, genome comparison and data visualisation. The portal will also be used to track the progress of the project for the community. 

## Portals
- Programm portal : https://www.atlasea.fr/
- Data portal : https://portal.atlasea.fr/


## Consortium description

## ABIMS plateform : Roscoff Marine Station , CNRS/Sorbonne Université
- Erwan CORRE (FR2424)
- Loraine GUEGUEN (UMR8227)
- Mark HOEBEKE (FR2424)
- Annie LEBRETON (FR2424) (10/2024 - 09/2027)
- Alexandre NICAISE (FR2424) (10/2024 - 09/2026)

Previous members
- Karine MASSAU (FR2424) (03/2024-09/2024)
- Arthur LE BARS (IFB) (03/2023-12/2024)

## DYOGEN team : Paris, IBENS, ENS/PSL, CNRS (UMR8197), INSERM (U1024)
- Alexandra LOUIS
- Hugues ROEST CROLLIUS
- Samuel LALAM
- Lucile JEUSSET

## GenOuest plateform : Rennes IRISA
- Anthony BRETAUDEAU (CNRS)
- Matéo BOUDET (INRAE)
- Théo FOULQUIER (CNRS)
- Romane LIBOUDAN (CNRS)

Previous members
- Olivier COLIN (CNRS)
- Olivier SALOU (Univ. Rennes)

## IFB : French Bioinformatic Institut (UAR 3601)
- Gildas LE CORGUILLE (SORBONNE UNIVERSITE)
- Julien SEILER (IFB - CNRS UAR 3601) 
- Nicole CHARRIERE (IFB - CNRS UAR 3601)

## SeBiMer team : Plouzané IFREMER 
- Patrick DURAND
- Yaelle PIHAN
- Laura LEROI
- Pauline AUFFRET

Previous members
- Alexandre CORMIER
